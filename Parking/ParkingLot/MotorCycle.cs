﻿using ParkingLot.Abstract;
using ParkingLot.Enums;

namespace ParkingLot
{
    public class Motorcycle : Vehicle
    {
        public Motorcycle()
        {
            Type = VehicleType.Motorcycle;
            Tariff = Settings.MotorcycleTariff;
        }

        public Motorcycle(double initBalance) : base(initBalance)
        {
            Type = VehicleType.Motorcycle;
            Tariff = Settings.MotorcycleTariff;
        }
    }
}