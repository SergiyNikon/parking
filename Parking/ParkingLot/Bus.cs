﻿using ParkingLot.Abstract;
using ParkingLot.Enums;

namespace ParkingLot
{
    public class Bus : Vehicle
    {
        public Bus()
        {
            Type = VehicleType.Bus;
            Tariff = Settings.BusTariff;
        }

        public Bus(double initBalance) : base(initBalance)
        {
            Type = VehicleType.Bus;
            Tariff = Settings.BusTariff;
        }
    }
}