﻿using ParkingLot.Abstract;
using ParkingLot.Enums;

namespace ParkingLot
{
    public class Truck : Vehicle
    {

        public Truck()
        {
            Type = VehicleType.Truck;
            Tariff = Settings.TruckTariff;
        }

        public Truck(double initBalance) : base(initBalance)
        {
            Type = VehicleType.Truck;
            Tariff = Settings.TruckTariff;
        }
    }
}