﻿using System;
using ParkingLot.Enums;
using ParkingLot.Interfaces;

namespace ParkingLot.Abstract
{
    public abstract class Vehicle: IVehicle, ITariff
    {
        private static int _currentId = 1;

        protected Vehicle()
        {
            Id = _currentId++;
            VehicleBalance = new Balance();
            CoefficientOfFine = Settings.CoefficientOfFine;
        }

        protected Vehicle(double balance)
        {
            Id = _currentId++;
            VehicleBalance = new Balance(balance);
            CoefficientOfFine = Settings.CoefficientOfFine;
        }

        public int Id { get; }
        public IBalance VehicleBalance { get; }
        public VehicleType Type { get; protected set; }
        public double Tariff { get; protected set; }
        public double CoefficientOfFine { get; protected set; }
    }
}