﻿namespace ParkingLot
{
    public class BalanceEventArgs
    {
        public double Amount { get; }

        public BalanceEventArgs(double amount)
        {
            Amount = amount;
        }
    }
}