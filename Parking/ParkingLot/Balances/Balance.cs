﻿using System;
using ParkingLot.Interfaces;

namespace ParkingLot
{
    public class Balance : IBalance
    {

        public delegate void BalanceEventHandler(object sender, BalanceEventArgs e);
        public static event BalanceEventHandler Received;
        public static event BalanceEventHandler Sent;
        public static event BalanceEventHandler Replenished;

        public Balance()
        {
            Value = 0;
        }

        public Balance(double initBalance)
        {
            if (initBalance >= 0)
                Value = initBalance;
            else
                throw new ArgumentException("Can not set the balance with a negative value");
        }

        public double Value { get; private set; }

        public void Receive(double value)
        {
            if (value >= 0)
            {
                Value += value;
            }
            else
            {
                throw new ArgumentException("Can not receive money with a negative value");
            }

            Received?.Invoke(this, new BalanceEventArgs(value));
        }

        public void Send(double value)
        {
            if (value >= 0)
            {
                Value -= value;
            }
            else
            {
                throw new ArgumentException("Can not send money with a negative value");
            }

            Sent?.Invoke(this, new BalanceEventArgs(value));
        }

        public void Replenish(double value)
        {
            if (value >= 0)
            {
                Value += value;
            }
            else
            {
                throw new ArgumentException("Can not replenish balance with a negative value");
            }
            Replenished?.Invoke(this, new BalanceEventArgs(value));
        }
    }
}