﻿using System.IO;

namespace ParkingLot
{
    public static class Settings
    {
        public static double InitParkingBalance { get; set; } = 0;
        public static int MaxParkingCapacity { get; set; } = 10;
        public static int PeriodicityOfWithdrawals { get; set; } = 5;
        public static double CarTariff { get; set; } = 2;
        public static double TruckTariff { get; set; } = 5;
        public static double BusTariff { get; set; } = 3.5;
        public static double MotorcycleTariff { get; set; } = 1;
        public static double CoefficientOfFine { get; set; } = 2.5;
        public static int PeriodicityOfLoggingToFile { get; set; } = 60;

        public static string DefaultLogFileName { get; set; } = Directory.GetParent(Directory.GetCurrentDirectory()).Parent?.Parent?.Parent?.FullName + @"\ParkingLot\Logs\Transactions.log";
    }
}