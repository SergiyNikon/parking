﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using ParkingLot.Abstract;
using ParkingLot.Interfaces;
using ParkingLot.Transactions;

namespace ParkingLot
{
    public class Logger
    {
        /// <summary>
        /// timer for writing to log file periodically
        /// </summary>
        private readonly Timer _timer;
        
        public Logger()
        {
            StringLogList = new List<string>();
            LogFileName = Settings.DefaultLogFileName;
            LogList = new List<TransactionEventArgs>();
            
            if (!File.Exists(LogFileName))
            {
                File.Create(LogFileName);
            }
            //clear log file
            File.WriteAllText(LogFileName, string.Empty);

            // subscription to event TransactionMade
            Transaction.TransactionMade += LogTransaction;

            // every {PeriodicityOfLoggingToFile} seconds log to file
            _timer = new Timer((e) => WriteLogToFileAndClear(), null, TimeSpan.Zero, TimeSpan.FromSeconds(Settings.PeriodicityOfLoggingToFile));
        }
        
        public string LogFileName { get; }

        /// <summary>
        /// last time deleted log list
        /// </summary>
        public List<TransactionEventArgs> LastLogList { get; set; }

        /// <summary>
        /// log list contains representation of transaction
        /// </summary>
        private List<TransactionEventArgs> LogList { get; }
        private List<string> StringLogList { get; }

        private void LogTransaction(object sender, TransactionEventArgs e)
        {
            LogList.Add(e);

            Vehicle vehicle = sender as Vehicle;
            
            StringBuilder sb = new StringBuilder();
            sb.Append("Transaction at ");
            sb.Append(e.TransactionTime);
            if (vehicle != null)
            {
                sb.Append(" from vehicle with Id ");
                sb.Append(vehicle.Id);
            }

            sb.Append(" With Amount = ");
            sb.Append(e.Amount);

            StringLogList.Add(sb.ToString());
        }

        private void WriteLogToFileAndClear()
        {
            File.AppendAllLines(LogFileName, StringLogList);
            LastLogList = new List<TransactionEventArgs>(LogList);
            StringLogList.Clear();
            LogList.Clear();
        }
    }
}