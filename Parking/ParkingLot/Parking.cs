﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using ParkingLot.Abstract;
using ParkingLot.Enums;
using ParkingLot.Interfaces;
using ParkingLot.Transactions;

namespace ParkingLot
{
    public sealed class Parking : IParking
    {
        /// <summary>
        /// timer for vehicles payment
        /// </summary>
        private readonly Timer _timer;

        //Singleton pattern implementation
        private Parking()
        {
            ParkingBalance = new Balance(Settings.InitParkingBalance);
            MaxCapacity = Settings.MaxParkingCapacity;
            ParkingLogger = new Logger();
            ParkedVehicles = new HashSet<Vehicle>();

            // every {PeriodicityOfWithdrawals} seconds take payment
            _timer = new Timer((e) => TakePayment(), null, TimeSpan.Zero, TimeSpan.FromSeconds(Settings.PeriodicityOfWithdrawals));
        }
        private static readonly Lazy<Parking> LazyInstance = new Lazy<Parking>(() => new Parking());
        public static Parking Instance => LazyInstance.Value;

        public HashSet<Vehicle> ParkedVehicles { get; }

        public IBalance ParkingBalance { get; }
        public int MaxCapacity { get; }
        public Logger ParkingLogger { get; }

        public ParkingStatus Park(Vehicle vehicle)
        {
            if (ParkedVehicles.Count < MaxCapacity)
            {
                if (ParkedVehicles.Contains(vehicle))
                {
                    throw new InvalidOperationException($"Vehicle with id {vehicle.Id} is already parked");
                }

                ParkedVehicles.Add(vehicle);
                return ParkingStatus.Success;
            }

            return ParkingStatus.Fail;
        }
        
        public ParkingStatus UnPark(Vehicle vehicle)
        {
            if (ParkedVehicles.Count > 0)
            {
                if (!ParkedVehicles.Contains(vehicle))
                {
                    throw new InvalidOperationException($"Vehicle with id {vehicle.Id} is not parked yet");
                }

                ParkedVehicles.Remove(vehicle);
                return ParkingStatus.Success;
            }

            return ParkingStatus.Fail;
        }

        public void TakePayment()
        {
            foreach (var v in ParkedVehicles)
            {
                Transaction.BeginTransaction(v, this);
            }
        }

        public Vehicle GetParkedVehicleById(int id)
        {
            return ParkedVehicles.FirstOrDefault(vehicle => vehicle.Id == id);

        }
    }
}