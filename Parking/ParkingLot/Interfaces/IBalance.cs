﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParkingLot.Interfaces
{
    public interface IBalance
    { 
        double Value { get; }

        void Receive(double value);
        void Send(double value);
        void Replenish(double value);
    }
}
