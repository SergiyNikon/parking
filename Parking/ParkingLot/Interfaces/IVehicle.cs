﻿using ParkingLot.Enums;

namespace ParkingLot.Interfaces
{
    public interface IVehicle
    {
        int Id { get; }
        VehicleType Type { get; }
    }
}
