﻿namespace ParkingLot.Interfaces
{
    public interface ITariff
    {
        double Tariff { get; }
        double CoefficientOfFine { get; }
    }
}