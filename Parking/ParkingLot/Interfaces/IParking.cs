﻿using ParkingLot.Abstract;
using ParkingLot.Enums;

namespace ParkingLot.Interfaces
{
    public interface IParking
    {
        int MaxCapacity { get; }

        ParkingStatus Park(Vehicle vehicle);
        ParkingStatus UnPark(Vehicle vehicle);
    }
}