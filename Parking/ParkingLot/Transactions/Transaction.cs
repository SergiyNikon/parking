﻿using System;
using System.Text;
using ParkingLot.Abstract;
using ParkingLot.Enums;
using ParkingLot.Interfaces;

namespace ParkingLot.Transactions
{
    public static class Transaction
    {
        public delegate void TransactionEventHandler(object sender, TransactionEventArgs e);
        public static event TransactionEventHandler TransactionMade;

        public static bool BeginTransaction(Vehicle from, Parking to)
        {
            try
            {
                double amount = from.Tariff;

                //a fine is imposed when not enough money on the vehicle balance
                if (amount > from.VehicleBalance.Value)
                {
                    amount *= from.CoefficientOfFine;
                }

                from.VehicleBalance.Send(amount);
                to.ParkingBalance.Receive(amount);


                TransactionMade?.Invoke(from, new TransactionEventArgs(amount, from));
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }
    }
}