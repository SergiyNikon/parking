﻿using System;
using System.Text;
using ParkingLot.Abstract;
using ParkingLot.Enums;

namespace ParkingLot.Transactions
{
    public class TransactionEventArgs : EventArgs
    {
        public DateTime TransactionTime { get; }
        public double Amount { get; }
        public Vehicle Vehicle { get; }

        public TransactionEventArgs(double amount, Vehicle vehicle)
        {
            TransactionTime = DateTime.Now;
            Amount = amount;
            Vehicle = vehicle;
        }
    }
}