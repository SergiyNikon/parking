﻿using ParkingLot.Abstract;
using ParkingLot.Enums;

namespace ParkingLot
{
    public class Car: Vehicle
    {

        public Car()
        {
            Type = VehicleType.Car;
            Tariff = Settings.CarTariff;
        }

        public Car(double initBalance) : base(initBalance)
        {
            Type = VehicleType.Car;
            Tariff = Settings.CarTariff;
        }
        
    }
}