﻿namespace ParkingLot.Enums
{
    public enum VehicleType
    {
        Car,
        Truck,
        Bus,
        Motorcycle
    }
}