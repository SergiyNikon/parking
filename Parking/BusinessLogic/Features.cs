﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BusinessLogic.Interfaces;
using ParkingLot;
using ParkingLot.Abstract;
using ParkingLot.Transactions;

namespace BusinessLogic
{
    public class Features : IFeature
    {
        private Parking _parking;

        public Features()
        {
            _parking = Parking.Instance;
        }

        public double GetCurrentParkingBalance()
        {
            return _parking.ParkingBalance.Value;
        }

        public double GetAmountOfEarnedMoneyForLastTime()
        {
            List<TransactionEventArgs> lastLogList = new List<TransactionEventArgs>(_parking.ParkingLogger.LastLogList);
            try
            {
                return lastLogList.Sum(item => item.Amount);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public int GetNumberOfVacantParkingSpots()
        {
            return _parking.MaxCapacity - _parking.ParkedVehicles.Count;
        }

        public List<TransactionEventArgs> GetAllTransactionsForLastTime()
        {
            return _parking.ParkingLogger.LastLogList;
        }

        public List<string> GetAllTransactions()
        {
            return File.ReadAllLines(_parking.ParkingLogger.LogFileName).ToList();
        }

        public HashSet<Vehicle> GetAllParkedVehicles()
        {
            return _parking.ParkedVehicles;
        }

        public void Park(Vehicle vehicle)
        {
            _parking.Park(vehicle);
        }

        public void UnPark(Vehicle vehicle)
        {
            _parking.UnPark(vehicle);
        }

        public void ReplenishVehicleBalance(Vehicle vehicle, double value)
        {
            vehicle.VehicleBalance.Replenish(value);
        }
    }
}