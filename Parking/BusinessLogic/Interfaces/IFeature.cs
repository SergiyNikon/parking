﻿using System.Collections.Generic;
using ParkingLot.Abstract;
using ParkingLot.Transactions;

namespace BusinessLogic.Interfaces
{
    public interface IFeature
    {
        double GetCurrentParkingBalance();
        double GetAmountOfEarnedMoneyForLastTime();
        int GetNumberOfVacantParkingSpots();
        List<TransactionEventArgs> GetAllTransactionsForLastTime();
        List<string> GetAllTransactions();
        HashSet<Vehicle> GetAllParkedVehicles();
        void Park(Vehicle vehicle);
        void UnPark(Vehicle vehicle);
        void ReplenishVehicleBalance(Vehicle vehicle, double value);
    }
}