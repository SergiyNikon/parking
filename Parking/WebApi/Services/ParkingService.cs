﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogic;
using BusinessLogic.Interfaces;
using Microsoft.AspNetCore.Identity.UI.Pages.Internal.Account;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ParkingLot.Abstract;

namespace WebApi.Services
{
    public class ParkingService
    {
        private readonly IFeature _feature = new Features();

        public string GetCurrentParkingBalance()
        {
            return _feature.GetCurrentParkingBalance().ToString();
        }

        public string GetAmountOfEarnedMoneyForLastTime()
        {
            return _feature.GetAmountOfEarnedMoneyForLastTime().ToString();
        }

        public string GetNumberOfVacantParkingSpots()
        {
            return _feature.GetNumberOfVacantParkingSpots().ToString();
        }

        public string GetAllTransactions()
        {
            return JsonConvert.SerializeObject(_feature.GetAllTransactions());
        }

        public void Park(Vehicle vehicle)
        {
            _feature.Park(vehicle);
        }

        public void UnPark(Vehicle vehicle)
        {
            _feature.UnPark(vehicle);
        }

        public void ReplenishVehicleBalance(Vehicle vehicle, double value)
        {
            _feature.ReplenishVehicleBalance(vehicle, value);
        }
    }
}
