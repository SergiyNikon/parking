﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogic;
using BusinessLogic.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ParkingLot.Abstract;
using WebApi.Services;


namespace WebApi.Controllers
{
    public class ParkingController : ControllerBase
    {
        private readonly ParkingService _parkingService;

        public ParkingController(ParkingService service)
        {
            _parkingService = service;
        }

        [HttpGet]
        public ActionResult<string> GetCurrentParkingBalance()
        {
            return _parkingService.GetCurrentParkingBalance();
        }

        [HttpGet]
        public ActionResult<string> GetAmountOfEarnedMoneyForLastTime()
        {
            return _parkingService.GetAmountOfEarnedMoneyForLastTime();
        }

        [HttpGet]
        public ActionResult<string> GetNumberOfVacantParkingSpots()
        {
            return _parkingService.GetNumberOfVacantParkingSpots();
        }

        [HttpGet]
        public ActionResult<string> GetAllTransactions()
        {
            return  _parkingService.GetAllTransactions();
        }

        [HttpPost]
        public void Park(Vehicle vehicle)
        {
            _parkingService.Park(vehicle);
        }

        [HttpPost]
        public void UnPark(Vehicle vehicle)
        {
            _parkingService.UnPark(vehicle);
        }

        [HttpPost]
        public void ReplenishVehicleBalance(Vehicle vehicle, double value)
        {
            _parkingService.ReplenishVehicleBalance(vehicle, value);
        }
    }
}