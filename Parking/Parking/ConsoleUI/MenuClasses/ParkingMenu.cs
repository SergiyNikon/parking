﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using BusinessLogic;
using BusinessLogic.Interfaces;
using ParkingLot;
using ParkingLot.Abstract;
using ParkingLot.Enums;

namespace ConsoleUI.MenuClasses
{
    public sealed class ParkingMenu : Menu
    {
        private readonly IFeature _features;
        private readonly Parking _parking;
        public ParkingMenu()
        {
            _features = new Features();
            _parking = Parking.Instance;
            MenuLoop();
        }

        protected override void AddMenuItems()
        {
            MenuItems.Add(nameof(IFeature.GetCurrentParkingBalance));
            MenuItems.Add(nameof(IFeature.GetAmountOfEarnedMoneyForLastTime));
            MenuItems.Add(nameof(IFeature.GetNumberOfVacantParkingSpots));
            MenuItems.Add(nameof(IFeature.GetAllTransactionsForLastTime));
            MenuItems.Add(nameof(IFeature.GetAllTransactions));
            MenuItems.Add(nameof(IFeature.GetAllParkedVehicles));
            MenuItems.Add(nameof(IFeature.Park));
            MenuItems.Add(nameof(IFeature.UnPark));
            MenuItems.Add(nameof(IFeature.ReplenishVehicleBalance));
        }

        public override void CallMethods(string s)
        {
            if (s == nameof(IFeature.GetCurrentParkingBalance))
            {
                var balance = _features.GetCurrentParkingBalance();
                Console.WriteLine("Current parking balance: " + balance);
            }
            else if (s == nameof(IFeature.GetAmountOfEarnedMoneyForLastTime))
            {
                var amount = _features.GetAmountOfEarnedMoneyForLastTime();
                Console.WriteLine("Amount of earned money for last time: " + amount);
            }
            else if (s == nameof(IFeature.GetNumberOfVacantParkingSpots))
            {
                var numberOfVacantParkingSpots = _features.GetNumberOfVacantParkingSpots();
                Console.WriteLine("Number fo vacant parking spots: " + numberOfVacantParkingSpots);
            }
            else if (s == nameof(IFeature.GetAllTransactionsForLastTime))
            {
                var transactions = _features.GetAllTransactionsForLastTime();
                Console.WriteLine($"Transactions for the last {Settings.PeriodicityOfLoggingToFile} seconds: ");
                StringBuilder sb;
                foreach (var transaction in transactions)
                {
                    sb = new StringBuilder();
                    sb.Append("Transaction at ");
                    sb.Append(transaction.TransactionTime);
                    sb.Append(" from vehicle with Id ");
                    sb.Append(transaction.Vehicle.Id);
                    sb.Append(" With Amount = ");
                    sb.Append(transaction.Amount);
                    Console.WriteLine(sb.ToString());
                }
            }
            else if (s == nameof(IFeature.GetAllTransactions))
            {
                var allTransactions = _features.GetAllTransactions();
                Console.WriteLine($"Transactions for all time: ");
                allTransactions.ForEach(Console.WriteLine);
            }
            else if (s == nameof(IFeature.GetAllParkedVehicles))
            {
                var parkedVehicles = _features.GetAllParkedVehicles();
                Console.WriteLine($"Parked vehicles: ");
                foreach (var parkedVehicle in parkedVehicles)
                {
                    Console.WriteLine($"{parkedVehicle.Type} with id {parkedVehicle.Id}");
                }
            }
            else if (s == nameof(IFeature.Park))
            {
                Console.WriteLine("What type of car are you parking? press buttons: ");
                VehicleParkingMenu vehicleParkingMenu = new VehicleParkingMenu();
                var vehicle = vehicleParkingMenu.GetVehicle();

                if (vehicle != null)
                {
                    _features.Park(vehicle);
                    Console.WriteLine($"{vehicle.Type} with {vehicle.Id} parked successfully!");
                }
                else
                {
                    Console.WriteLine("Fail to park vehicle");
                }
            }
            else if (s == nameof(IFeature.UnPark))
            {
                Console.WriteLine("Enter vehicle id to UnPark: ");
                int.TryParse(Console.ReadLine(), out int id);
                var vehicleToRemove = _parking.GetParkedVehicleById(id);
                if (vehicleToRemove != null)
                {
                    _features.UnPark(vehicleToRemove);
                    Console.WriteLine($"{vehicleToRemove.Type} with {vehicleToRemove.Id} unparked successfully!");
                }
                else
                {
                    Console.WriteLine("Vehicle with id {id} is not parked yet");
                }
            }
            
            else if (s == nameof(IFeature.ReplenishVehicleBalance))
            {
                Console.WriteLine("Enter vehicle id to Replenish balance to");
                int.TryParse(Console.ReadLine(), out int id);
                var vehicleToReplenish = _parking.GetParkedVehicleById(id);
                if (vehicleToReplenish != null) { 
                    Console.WriteLine("Enter value to replenish");
                    double.TryParse(Console.ReadLine(), out double value);
                    _features.ReplenishVehicleBalance(vehicleToReplenish, value);
                    Console.WriteLine(
                        $"Current balance in vehicle with id {vehicleToReplenish.Id}: {vehicleToReplenish.VehicleBalance.Value}");
                }
                else
                {
                    Console.WriteLine($"Vehicle with id {id} is not parked yet");
                }
            }
        }
    }
}