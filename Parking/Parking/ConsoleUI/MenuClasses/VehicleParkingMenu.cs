﻿using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using ParkingLot;
using ParkingLot.Abstract;
using ParkingLot.Enums;

namespace ConsoleUI.MenuClasses
{
    public sealed class VehicleParkingMenu : Menu
    {
        private Vehicle _vehicle;

        public VehicleParkingMenu()
        {
            _vehicle = null;
            MenuLoop();
        }

        public Vehicle GetVehicle() => _vehicle;

        protected override void AddMenuItems()
        {
            MenuItems.Add($"{nameof(VehicleType.Car)}");
            MenuItems.Add($"{nameof(VehicleType.Bus)}");
            MenuItems.Add($"{nameof(VehicleType.Motorcycle)}");
            MenuItems.Add($"{nameof(VehicleType.Truck)}");
        }

        public override void CallMethods(string s)
        {
            switch (s)
            {
                case nameof(VehicleType.Car):
                    _vehicle = new Car();
                    break;
                case nameof(VehicleType.Bus):
                    _vehicle = new Bus();
                    break;
                case nameof(VehicleType.Motorcycle):
                    _vehicle = new Motorcycle();
                    break;
                case nameof(VehicleType.Truck):
                    _vehicle = new Truck();
                    break;
            }
        }

        public override void MenuLoop()
        {
            string res = ShowMenuDialog(MenuItems);
            CallMethods(res);
        }
    }
}