﻿using System;
using System.Collections.Generic;

namespace ConsoleUI.MenuClasses
{
    public sealed class ProgramMenu: Menu
    {
        private ParkingMenu _parkingMenu;
        private SettingsMenu _settingsMenu;

        public ProgramMenu()
        {
            MenuLoop();
        }

        protected override void AddMenuItems()
        {
            MenuItems.Add($"Parking");
            MenuItems.Add($"Settings");

        }

        public override void CallMethods(string s)
        {
            switch (s)
            {
                case "Parking":
                    _parkingMenu = new ParkingMenu();
                    break;
                case "Settings":
                    _settingsMenu = new SettingsMenu();
                    break;
            }
        }
    }
}