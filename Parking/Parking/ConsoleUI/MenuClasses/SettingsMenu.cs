﻿using System;
using System.Collections.Generic;
using ConsoleUI.Helpers;
using ConsoleUI.Interfaces;
using ParkingLot;

namespace ConsoleUI.MenuClasses
{
    public sealed class SettingsMenu : Menu
    {
        public SettingsMenu()
        {
            MenuLoop();
        }

        protected override void AddMenuItems()
        {
            MenuItems.Add(nameof(Settings.InitParkingBalance));
            MenuItems.Add(nameof(Settings.MaxParkingCapacity));
            MenuItems.Add(nameof(Settings.PeriodicityOfWithdrawals));
            MenuItems.Add(nameof(Settings.CarTariff));
            MenuItems.Add(nameof(Settings.TruckTariff));
            MenuItems.Add(nameof(Settings.BusTariff));
            MenuItems.Add(nameof(Settings.MotorcycleTariff));
            MenuItems.Add(nameof(Settings.CoefficientOfFine));
            MenuItems.Add(nameof(Settings.PeriodicityOfLoggingToFile));
            MenuItems.Add(nameof(Settings.DefaultLogFileName));
        }

        public override void CallMethods(string s)
        {
            if (s == nameof(Settings.InitParkingBalance))
            {
                Settings.InitParkingBalance = SettingsHelper.ChangeSetting(Settings.InitParkingBalance,
                    nameof(Settings.InitParkingBalance));
            }
            else if (s == nameof(Settings.MaxParkingCapacity))
            {
                Settings.MaxParkingCapacity = SettingsHelper.ChangeSetting(Settings.MaxParkingCapacity,
                    nameof(Settings.MaxParkingCapacity));
            }
            else if (s == nameof(Settings.PeriodicityOfWithdrawals))
            {
                Settings.PeriodicityOfWithdrawals = SettingsHelper.ChangeSetting(Settings.PeriodicityOfWithdrawals,
                    nameof(Settings.PeriodicityOfWithdrawals));
            }
            else if (s == nameof(Settings.CarTariff))
            {
                Settings.CarTariff = SettingsHelper.ChangeSetting(Settings.CarTariff, nameof(Settings.CarTariff));
            }
            else if (s == nameof(Settings.TruckTariff))
            {
                Settings.TruckTariff =
                    SettingsHelper.ChangeSetting(Settings.TruckTariff, nameof(Settings.TruckTariff));
            }
            else if (s == nameof(Settings.BusTariff))
            {
                Settings.BusTariff = SettingsHelper.ChangeSetting(Settings.BusTariff, nameof(Settings.BusTariff));
            }
            else if (s == nameof(Settings.MotorcycleTariff))
            {
                Settings.MotorcycleTariff =
                    SettingsHelper.ChangeSetting(Settings.MotorcycleTariff, nameof(Settings.MotorcycleTariff));
            }
            else if (s == nameof(Settings.CoefficientOfFine))
            {
                Settings.CoefficientOfFine = SettingsHelper.ChangeSetting(Settings.CoefficientOfFine,
                    nameof(Settings.CoefficientOfFine));
            }
            else if (s == nameof(Settings.PeriodicityOfLoggingToFile))
            {
                Settings.PeriodicityOfLoggingToFile = SettingsHelper.ChangeSetting(
                    Settings.PeriodicityOfLoggingToFile, nameof(Settings.PeriodicityOfLoggingToFile));
            }

            else if (s == nameof(Settings.DefaultLogFileName))
            {
                Settings.DefaultLogFileName = SettingsHelper.ChangeSetting(Settings.DefaultLogFileName,
                    nameof(Settings.DefaultLogFileName));
            }
            
        }
    }
}