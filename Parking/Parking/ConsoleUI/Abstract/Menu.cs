﻿using System;
using System.Collections.Generic;
using System.Resources;
using ConsoleUI.Interfaces;
using ConsoleUI.MenuClasses;

namespace ConsoleUI
{
    public class Menu : IMenu
    {
        private const int SpacingPerLine = 40;

        public Menu()
        {
            MenuItems = new List<string>();
            MenuItems.Add("<-Back");
            AddMenuItems();
        }

        public List<string> MenuItems { get; set; }

        public string ShowMenuDialog(List<string> items)
        {
            

            Console.CursorVisible = false;
            ConsoleKeyInfo key;
            int arrowIndex = 0;
            bool isExit = false;
            do
            {
                Console.Clear();

                //write menu class name at the top of the screen in uppercase
                Console.WriteLine("---> " + this.GetType().Name.ToUpper() + " <---");
                Console.WriteLine();

                for (int i = 0; i < items.Count; i++)
                {
                    if (arrowIndex == i)
                    {
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.BackgroundColor = ConsoleColor.Gray;
                        Console.WriteLine(">" + items[i].PadRight(SpacingPerLine));
                    }
                    else
                    {
                        Console.WriteLine(" " + items[i].PadRight(SpacingPerLine));
                    }

                    Console.ResetColor();
                }
                key = Console.ReadKey();
                switch (key.Key)
                {
                    case ConsoleKey.DownArrow:
                        arrowIndex = arrowIndex == items.Count - 1 ? 0 : arrowIndex + 1;
                        break;

                    case ConsoleKey.UpArrow:
                        arrowIndex = arrowIndex == 0 ? items.Count - 1 : arrowIndex - 1;
                        break;

                    case ConsoleKey.Enter:
                        Console.Clear();
                        Console.CursorVisible = true;
                        return items[arrowIndex];

                    case ConsoleKey.Escape:
                        isExit = true;
                        break;
                }
            } while (!isExit);
            Console.Clear();
            Console.CursorVisible = true;
            return null;

        }

        public virtual void CallMethods(string s)
        {

        }

        protected virtual void AddMenuItems()
        {

        }

        public virtual void MenuLoop()
        {
            string res;
            do
            {
                res = ShowMenuDialog(MenuItems);
                CallMethods(res);
                //don't write press any key to continue when it's main program menu and 'button' back not pressed
                if (res != "<-Back" && this.GetType() != typeof(ProgramMenu))
                {
                    Console.Write("Press any key to continue...");
                    Console.ReadKey();
                }
            } while (res != "<-Back");
        }
    }
}