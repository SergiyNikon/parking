﻿using System;
using System.Collections.Generic;
using System.Threading;
using BusinessLogic.Interfaces;
using ConsoleUI.Helpers;
using ConsoleUI.MenuClasses;
using ParkingLot;
using ParkingLot.Abstract;
using ParkingLot.Interfaces;
using ParkingLot.Transactions;

namespace ConsoleUI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("To move the arrow use up arrow button and down arrow button.");
            Console.Write("Press any key to continue...");
            Console.ReadKey();
            ProgramMenu programMenu = new ProgramMenu();
        }
    }
}