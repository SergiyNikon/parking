﻿using System.Collections.Generic;

namespace ConsoleUI.Interfaces
{
    public interface IMenu
    {
        List<string> MenuItems { get; set; }
        string ShowMenuDialog(List<string> items);
        void CallMethods(string s);
        void MenuLoop();
    }
}