﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using ParkingLot;

namespace ConsoleUI.Helpers
{
    public static class SettingsHelper
    {
        public static double ChangeSetting(double setting, string settingName)
        {
            Console.WriteLine($"Enter new value to {settingName}: ");
            string value = Console.ReadLine();
            if (value == "")
            {
                Console.WriteLine("Cancel.");
                return setting;
            }
            bool success = double.TryParse(value, out double result);
            if (success)
            {
                Console.WriteLine($"Now {settingName} is {result}");
                return result;
            }
            Console.WriteLine($"Fail to change {settingName}!");
            return setting;
        }

        public static int ChangeSetting(int setting, string settingName)
        {
            Console.WriteLine($"Enter new value to {settingName}: ");
            string value = Console.ReadLine();
            if (value == "")
            {
                Console.WriteLine("Cancel.");
                return setting;
            }
            bool success = int.TryParse(value, out int result);
            if (success)
            {
                Console.WriteLine($"Now {settingName} is {result}");
                return result;
            }
            Console.WriteLine($"Fail to change {settingName}!");
            return setting;
        }

        public static string ChangeSetting(string setting, string settingName)
        {
            Console.WriteLine($"Enter new value to {settingName}: ");
            string result = Console.ReadLine();
            if (result == "")
            {
                Console.WriteLine("Cancel.");
                return setting;
            }
            Console.WriteLine($"Now {settingName} is {result}");
            return setting;
        }
    }
}